# Simple gallery plugin for my pelican blog

A plugin for gallery that will present my photos on my personal blog. 

Idea is that I'll just dump photos into a folder, and then this plugin will
read EXIF metadata and use names and descriptions to present nice formatted
gallery with thumbnails and whatnot. 

Current status: 

* NOTHING WORKS 

What I know: 

* Probably I'll need to create pelican plugin: http://docs.getpelican.com/en/3.6.3/plugins.html#where-to-find-plugins

  
* Passing context to Rest Directive --- rest directives are like 20 frames below
  pelican in stack. Only way to pass context to them is to use: DOCUTILS_SETTINGS
  and then self.state_machine.document.settings will contain DOCUTILS_SETTINGS.
  
  register signal on initialized and hijack settings from there  

Research required: 

* How can I dynamically add static files to pelican (thumbnails)
  
  register static_generator_init 
  
* How 

TODO: 

* Create abstract class for gallery 
* Create rest directive that implements gallery 
* Create pelican plugin that automatically configures settings (and or RestReader and or add own reader to override built-in one)


Longterm goals: 

* Allow downloading images from swift or s3 during build so we won't kill
  git repo with images. 
* Create thumbnails
* Clean EXIF tags 

    