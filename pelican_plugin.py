# coding=utf-8
import tempfile
import typing

import pathlib

from docutils.parsers.rst import directives
from pelican import signals, Pelican, StaticGenerator
from pip._vendor.distlib._backport import shutil

from docutils_gallery.gallery_directive import GalleryDirective

from . import api


class PelicanPlugin(object):

  def __init__(self, settings: typing.Optional[api.GallerySettings]):
    self.settings = settings
    self.cleanups: typing.List[typing.Callable[[], None]] = []

  def __initialize_settings(self, pelican):
    if self.settings is None:
      self.settings = api.GallerySettings(
        gallery_images_search_path=pelican.settings['PATH']
      )
    self.settings = self.settings._replace(
      generated_images_path = self.__normalize_generated_images_path(
        self.settings.generated_images_path
      )
    )
    return self.settings

  def __normalize_generated_images_path(self, to_normalize):
    if to_normalize is None:
      temp_file = pathlib.Path(tempfile.mkdtemp())
      def cleanup():
        shutil.rmtree(str(temp_file))
      self.cleanups.append(cleanup)
      return temp_file
    if isinstance(to_normalize, str):
      return pathlib.Path(to_normalize)
    return to_normalize

  def register(self):
    signals.initialized.connect(self.initialized)
    signals.finalized.connect(self.finalized)
    signals.static_generator_init(self.initialize_static_files_generator)

  def finalized(self, pelican):
    # TODO: We should de-initialize GalleryDirective but there is not API to do that
    for cleanup in self.cleanups:
      cleanup()

  def __install_directive(self):
    directives.register_directive("gallery", GalleryDirective)

  def __install_settings_to_pelican(self, pelican):
    docutils_settings = pelican.settings.get('DOCUTILS_SETTINGS', {})
    docutils_settings['GALLERY_PLUGIN'] = api.GalleryPluginState(
      settings=self.settings,
    )
    pelican.settings['DOCUTILS_SETTINGS'] = docutils_settings

  def initialized(self, pelican):
    self.settings = self.__initialize_settings(pelican)
    self.__install_settings_to_pelican(pelican)
    self.__install_directive()

  def initialize_static_files_generator(self, generator: StaticGenerator):
    generator_settings = dict(**generator.settings)
    static_paths = generator_settings['STATIC_PATHS']
    if static_paths is None:
      static_paths = []
    static_paths.append(self.settings.generated_images_path)
    generator.settings = generator_settings