# coding=utf-8

import pathlib

from docutils import nodes, utils
from docutils.parsers.rst import Directive, directives, roles

from . import api, utils


class GalleryRenderer(object):
  """
  Abstract class
  """


class GalleryDirective(Directive):

  required_arguments = 1

  PELICAN_FILENAME_TAG = "{filename}"

  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.plugin_state: api.GalleryPluginState = None
    self.state: api.GalleryState = None

  def initialize_state(self) -> api.GalleryState:
    source = pathlib.Path(self.arguments[0])

  def run(self):

    self.plugin_state = utils.get_state(self)



    return [nodes.raw('', "<p>{}</p>".format(uri), format='html')]




