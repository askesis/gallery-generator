# coding=utf-8

import pathlib
import typing


from . import api

if typing.TYPE_CHECKING:

  from docutils.parsers.rst import Directive
  from pelican import Pelican


def get_state(obj: 'typing.Union[Directive, Pelican]') -> 'api.GalleryPluginState':
  if isinstance(obj, Directive):
    return obj.state_machine.document.settings['GALLERY_PLUGIN']
  if isinstance(obj, Pelican):
    return obj.settings['GALLERY_PLUGIN']
