# coding=utf-8

import typing
import pathlib


class GallerySettings(typing.NamedTuple):

  """
  Settings that user can update.
  """

  gallery_images_search_path: typing.Union[typing.Sequence[str], str] = None
  generated_images_path: typing.Union[None, str, pathlib.Path] = None


class GalleryPluginState(typing.NamedTuple):

  """
  Internal object containing whole plugin state.
  """

  settings: GallerySettings


class GalleryState(typing.NamedTuple):
  """
  State for a gallery directive.
  """

  source_folder: pathlib.Path
  dest_folder: pathlib.Path
  images: typing.Sequence['ImageDescription'] = []


class ImageDescription(typing.NamedTuple):

  original_file: pathlib.Path

  short_description: str
  long_description: str
